﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.RotationChartVMs
{
    public partial class RotationChartListVM : BasePagedListVM<RotationChart_View, RotationChartSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RotationChart", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<RotationChart_View>> InitGridHeader()
        {
            return new List<GridColumn<RotationChart_View>>{
                this.MakeGridHeader(x => x.PhotoId).SetFormat(PhotoIdFormat),
                this.MakeGridHeader(x => x.Title_view),
                this.MakeGridHeaderAction(width: 200)
            };
        }
        private List<ColumnFormatInfo> PhotoIdFormat(RotationChart_View entity, object val)
        {
            return new List<ColumnFormatInfo>
            {
                ColumnFormatInfo.MakeDownloadButton(ButtonTypesEnum.Button,entity.PhotoId),
                ColumnFormatInfo.MakeViewButton(ButtonTypesEnum.Button,entity.PhotoId,640,480),
            };
        }


        public override IOrderedQueryable<RotationChart_View> GetSearchQuery()
        {
            var query = DC.Set<RotationChart>()
                .CheckEqual(Searcher.ExaminationSetupID, x=>x.ExaminationSetupID)
                .Select(x => new RotationChart_View
                {
				    ID = x.ID,
                    PhotoId = x.PhotoId,
                    Title_view = x.ExaminationSetup.Title,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class RotationChart_View : RotationChart{
        [Display(Name = "试卷名称")]
        public String Title_view { get; set; }

    }
}
