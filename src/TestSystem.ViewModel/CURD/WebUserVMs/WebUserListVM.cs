﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WebUserVMs
{
    public partial class WebUserListVM : BasePagedListVM<WebUser_View, WebUserSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WebUser", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<WebUser_View>> InitGridHeader()
        {
            return new List<GridColumn<WebUser_View>>{
                this.MakeGridHeader(x => x.UnitWorkName_view),
                this.MakeGridHeader(x => x.ITCode),
                this.MakeGridHeader(x => x.Name),
                this.MakeGridHeader(x => x.CellPhone),
                this.MakeGridHeader(x => x.HomePhone),
                this.MakeGridHeader(x => x.PhotoId).SetFormat(PhotoIdFormat),
                this.MakeGridHeader(x => x.IsValid),
                this.MakeGridHeader(x => x.RoleName_view),
                this.MakeGridHeaderAction(width: 200)
            };
        }
        private List<ColumnFormatInfo> PhotoIdFormat(WebUser_View entity, object val)
        {
            return new List<ColumnFormatInfo>
            {
                ColumnFormatInfo.MakeDownloadButton(ButtonTypesEnum.Button,entity.PhotoId),
                ColumnFormatInfo.MakeViewButton(ButtonTypesEnum.Button,entity.PhotoId,640,480),
            };
        }


        public override IOrderedQueryable<WebUser_View> GetSearchQuery()
        {
            var query = DC.Set<WebUser>()
                .CheckEqual(Searcher.UnitWorkID, x=>x.UnitWorkID)
                .CheckContain(Searcher.ITCode, x=>x.ITCode)
                .CheckContain(Searcher.Name, x=>x.Name)
                .Select(x => new WebUser_View
                {
				    ID = x.ID,
                    UnitWorkName_view = x.UnitWork.UnitWorkName,
                    ITCode = x.ITCode,
                    Name = x.Name,
                    CellPhone = x.CellPhone,
                    HomePhone = x.HomePhone,
                    PhotoId = x.PhotoId,
                    IsValid = x.IsValid,
                    RoleName_view = x.UserRoles.Select(y=>y.Role.RoleName).ToSpratedString(null,","), 
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class WebUser_View : WebUser{
        [Display(Name = "部门名称")]
        public String UnitWorkName_view { get; set; }
        [Display(Name = "RoleName")]
        public String RoleName_view { get; set; }

    }
}
