﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WebUserVMs
{
    public partial class WebUserSearcher : BaseSearcher
    {
        public List<ComboSelectListItem> AllUnitWorks { get; set; }
        [Display(Name = "部门名称")]
        public Guid? UnitWorkID { get; set; }
        [Display(Name = "Account")]
        public String ITCode { get; set; }
        [Display(Name = "Name")]
        public String Name { get; set; }

        protected override void InitVM()
        {
            AllUnitWorks = DC.Set<UnitWork>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.UnitWorkName);
        }

    }
}
