﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WebUserVMs
{
    public partial class WebUserBatchVM : BaseBatchVM<WebUser, WebUser_BatchEdit>
    {
        public WebUserBatchVM()
        {
            ListVM = new WebUserListVM();
            LinkedVM = new WebUser_BatchEdit();
        }

    }

	/// <summary>
    /// Class to define batch edit fields
    /// </summary>
    public class WebUser_BatchEdit : BaseVM
    {

        protected override void InitVM()
        {
        }

    }

}
