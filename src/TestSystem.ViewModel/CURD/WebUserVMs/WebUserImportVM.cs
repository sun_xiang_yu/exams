﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WebUserVMs
{
    public partial class WebUserTemplateVM : BaseTemplateVM
    {
        [Display(Name = "部门名称")]
        public ExcelPropety UnitWork_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.UnitWorkID);
        [Display(Name = "Account")]
        public ExcelPropety ITCode_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.ITCode);
        [Display(Name = "Password")]
        public ExcelPropety Password_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.Password);
        [Display(Name = "Name")]
        public ExcelPropety Name_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.Name);
        [Display(Name = "CellPhone")]
        public ExcelPropety CellPhone_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.CellPhone);
        [Display(Name = "HomePhone")]
        public ExcelPropety HomePhone_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.HomePhone);
        [Display(Name = "IsValid")]
        public ExcelPropety IsValid_Excel = ExcelPropety.CreateProperty<WebUser>(x => x.IsValid);

	    protected override void InitVM()
        {
            UnitWork_Excel.DataType = ColumnDataType.ComboBox;
            UnitWork_Excel.ListItems = DC.Set<UnitWork>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.UnitWorkName);
        }

    }

    public class WebUserImportVM : BaseImportVM<WebUserTemplateVM, WebUser>
    {

    }

}
