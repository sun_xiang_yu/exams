﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WrongQuestionBankVMs
{
    public partial class WrongQuestionBankTemplateVM : BaseTemplateVM
    {
        [Display(Name = "题目标识")]
        public ExcelPropety QuestionID_Excel = ExcelPropety.CreateProperty<WrongQuestionBank>(x => x.QuestionID);
        [Display(Name = "考试用户")]
        public ExcelPropety WebUser_Excel = ExcelPropety.CreateProperty<WrongQuestionBank>(x => x.WebUserID);
        [Display(Name = "做错次数")]
        public ExcelPropety WrongNumber_Excel = ExcelPropety.CreateProperty<WrongQuestionBank>(x => x.WrongNumber);

	    protected override void InitVM()
        {
            WebUser_Excel.DataType = ColumnDataType.ComboBox;
            WebUser_Excel.ListItems = DC.Set<WebUser>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Name);
        }

    }

    public class WrongQuestionBankImportVM : BaseImportVM<WrongQuestionBankTemplateVM, WrongQuestionBank>
    {

    }

}
