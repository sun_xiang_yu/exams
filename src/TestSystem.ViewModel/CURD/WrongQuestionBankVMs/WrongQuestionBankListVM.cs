﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WrongQuestionBankVMs
{
    public partial class WrongQuestionBankListVM : BasePagedListVM<WrongQuestionBank_View, WrongQuestionBankSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("WrongQuestionBank", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<WrongQuestionBank_View>> InitGridHeader()
        {
            return new List<GridColumn<WrongQuestionBank_View>>{
                this.MakeGridHeader(x => x.QuestionID),
                this.MakeGridHeader(x => x.Name_view),
                this.MakeGridHeader(x => x.WrongNumber),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<WrongQuestionBank_View> GetSearchQuery()
        {
            var query = DC.Set<WrongQuestionBank>()
                .CheckEqual(Searcher.WebUserID, x=>x.WebUserID)
                .Select(x => new WrongQuestionBank_View
                {
				    ID = x.ID,
                    QuestionID = x.QuestionID,
                    Name_view = x.WebUser.Name,
                    WrongNumber = x.WrongNumber,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class WrongQuestionBank_View : WrongQuestionBank{
        [Display(Name = "Name")]
        public String Name_view { get; set; }

    }
}
