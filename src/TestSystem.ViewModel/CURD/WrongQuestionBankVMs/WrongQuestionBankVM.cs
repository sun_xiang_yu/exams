﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.WrongQuestionBankVMs
{
    public partial class WrongQuestionBankVM : BaseCRUDVM<WrongQuestionBank>
    {
        public List<ComboSelectListItem> AllWebUsers { get; set; }

        public WrongQuestionBankVM()
        {
            SetInclude(x => x.WebUser);
        }

        protected override void InitVM()
        {
            AllWebUsers = DC.Set<WebUser>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Name);
        }

        public override void DoAdd()
        {           
            base.DoAdd();
        }

        public override void DoEdit(bool updateAllFields = false)
        {
            base.DoEdit(updateAllFields);
        }

        public override void DoDelete()
        {
            base.DoDelete();
        }
    }
}
