﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.ExaminationSetupVMs
{
    public partial class ExaminationSetupSearcher : BaseSearcher
    {
        [Display(Name = "试卷名称")]
        public String Title { get; set; }
        [Display(Name = "开始时间")]
        public DateRange StrTime { get; set; }
        [Display(Name = "科目")]
        public String Subject { get; set; }

        protected override void InitVM()
        {
        }

    }
}
