﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;
using TestSystem.Model.FrameworkEnumeration;


namespace TestSystem.ViewModel.CURD.ExaminationSetupVMs
{
    public partial class ExaminationSetupListVM : BasePagedListVM<ExaminationSetup_View, ExaminationSetupSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("ExaminationSetup", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<ExaminationSetup_View>> InitGridHeader()
        {
            return new List<GridColumn<ExaminationSetup_View>>{
                this.MakeGridHeader(x => x.Title),
                this.MakeGridHeader(x=>x.ParticipationTypes),
                this.MakeGridHeader(x => x.StrTime),
                this.MakeGridHeader(x => x.EndTime),
                this.MakeGridHeader(x => x.CourseEnum),
                this.MakeGridHeader(x => x.DXNumer),
                this.MakeGridHeader(x => x.DXScore),
                this.MakeGridHeader(x => x.DSXNumer),
                this.MakeGridHeader(x => x.DSXScore),
                this.MakeGridHeader(x => x.PDNumer),
                this.MakeGridHeader(x => x.PDScore),
                this.MakeGridHeader(x => x.TestTime),
                this.MakeGridHeader(x => x.Name_view),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<ExaminationSetup_View> GetSearchQuery()
        {
            var query = DC.Set<ExaminationSetup>()
                .CheckContain(Searcher.Title, x=>x.Title)
                .CheckBetween(Searcher.StrTime?.GetStartTime(), Searcher.StrTime?.GetEndTime(), x => x.StrTime, includeMax: false)
                .CheckContain(Searcher.Subject, x=>x.Subject)
                .Select(x => new ExaminationSetup_View
                {
                    ID = x.ID,
                    Title = x.Title,
                    ParticipationTypes =x.ParticipationTypes,
                    StrTime = x.StrTime,
                    EndTime = x.EndTime,
                    CourseEnum = x.CourseEnum,
                    DXNumer = x.DXNumer,
                    DXScore = x.DXScore,
                    DSXNumer = x.DSXNumer,
                    DSXScore = x.DSXScore,
                    PDNumer = x.PDNumer,
                    PDScore = x.PDScore,
                    TestTime = x.TestTime,
                    Name_view = x.Seal.Name,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class ExaminationSetup_View : ExaminationSetup{
        [Display(Name = "单位名称")]
        public String Name_view { get; set; }

    }
}
