﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.RecordNoAccountVMs
{
    public partial class RecordNoAccountListVM : BasePagedListVM<RecordNoAccount_View, RecordNoAccountSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                //this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                //this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                //this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                //this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                //this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordNoAccount", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<RecordNoAccount_View>> InitGridHeader()
        {
            return new List<GridColumn<RecordNoAccount_View>>{
                this.MakeGridHeader(x => x.Title_view),
                this.MakeGridHeader(x => x.ParticipationTime),
                this.MakeGridHeader(x => x.UserName),
                this.MakeGridHeader(x => x.UnitWork),
                this.MakeGridHeader(x => x.Phone),
                this.MakeGridHeader(x => x.Achievement),
                this.MakeGridHeader(x => x.QuestionId),
                this.MakeGridHeader(x => x.QuestionAnswer),
                this.MakeGridHeader(x => x.ExamineeAnswers),
                
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<RecordNoAccount_View> GetSearchQuery()
        {
            var query = DC.Set<RecordNoAccount>()
                .CheckEqual(Searcher.ExaminationSetupID, x=>x.ExaminationSetupID)
                .CheckContain(Searcher.UserName, x=>x.UserName)
                .CheckContain(Searcher.UnitWork, x=>x.UnitWork)
                .CheckContain(Searcher.Phone, x=>x.Phone)
                .Select(x => new RecordNoAccount_View
                {
				    ID = x.ID,
                    Title_view = x.ExaminationSetup.Title,
                    ParticipationTime = x.ParticipationTime,
                    UserName = x.UserName,
                    UnitWork = x.UnitWork,
                    Phone = x.Phone,
                    Achievement = x.Achievement,
                    QuestionId = x.QuestionId,
                    QuestionAnswer = x.QuestionAnswer,
                    ExamineeAnswers = x.ExamineeAnswers,
                    
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class RecordNoAccount_View : RecordNoAccount{
        [Display(Name = "试卷名称")]
        public String Title_view { get; set; }

    }
}
