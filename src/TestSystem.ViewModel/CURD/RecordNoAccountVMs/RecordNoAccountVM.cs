﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.RecordNoAccountVMs
{
    public partial class RecordNoAccountVM : BaseCRUDVM<RecordNoAccount>
    {
        public List<ComboSelectListItem> AllExaminationSetups { get; set; }

        public RecordNoAccountVM()
        {
            SetInclude(x => x.ExaminationSetup);
        }

        protected override void InitVM()
        {
            AllExaminationSetups = DC.Set<ExaminationSetup>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Title);
        }

        public override void DoAdd()
        {           
            base.DoAdd();
        }

        public override void DoEdit(bool updateAllFields = false)
        {
            base.DoEdit(updateAllFields);
        }

        public override void DoDelete()
        {
            base.DoDelete();
        }
    }
}
