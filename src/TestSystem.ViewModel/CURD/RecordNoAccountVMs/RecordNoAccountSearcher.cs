﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.RecordNoAccountVMs
{
    public partial class RecordNoAccountSearcher : BaseSearcher
    {
        public List<ComboSelectListItem> AllExaminationSetups { get; set; }
        [Display(Name = "参与试卷")]
        public int? ExaminationSetupID { get; set; }
        [Display(Name = "参与人")]
        public String UserName { get; set; }
        [Display(Name = "所在单位")]
        public String UnitWork { get; set; }
        [Display(Name = "手机号码")]
        public String Phone { get; set; }

        protected override void InitVM()
        {
            AllExaminationSetups = DC.Set<ExaminationSetup>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Title);
        }

    }
}
