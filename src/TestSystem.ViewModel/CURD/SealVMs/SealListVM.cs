﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.SealVMs
{
    public partial class SealListVM : BasePagedListVM<Seal_View, SealSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
               this.MakeStandardAction("Seal", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),

                this.MakeStandardAction("Seal", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD",dialogWidth: 800),
                this.MakeStandardAction("Seal", GridActionStandardTypesEnum.Details, Localizer["Details"],"CURD", dialogWidth: 800),
            };
        }


        protected override IEnumerable<IGridColumn<Seal_View>> InitGridHeader()
        {
            return new List<GridColumn<Seal_View>>{
                this.MakeGridHeader(x => x.Name),
                this.MakeGridHeader(x => x.SType),
                this.MakeGridHeader(x => x.PhotoId).SetFormat(PhotoIdFormat),
                this.MakeGridHeaderAction(width: 200)
            };
        }
        private List<ColumnFormatInfo> PhotoIdFormat(Seal_View entity, object val)
        {
            return new List<ColumnFormatInfo>
            {
                ColumnFormatInfo.MakeDownloadButton(ButtonTypesEnum.Button,entity.PhotoId),
                ColumnFormatInfo.MakeViewButton(ButtonTypesEnum.Button,entity.PhotoId,640,480),
            };
        }


        public override IOrderedQueryable<Seal_View> GetSearchQuery()
        {
            var query = DC.Set<Seal>()
                .CheckContain(Searcher.Name, x=>x.Name)
                .Select(x => new Seal_View
                {
				    ID = x.ID,
                    Name = x.Name,
                    SType = x.SType,
                    PhotoId = x.PhotoId,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class Seal_View : Seal{

    }
}
