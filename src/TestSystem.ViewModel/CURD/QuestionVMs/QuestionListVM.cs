﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.QuestionVMs
{
    public partial class QuestionListVM : BasePagedListVM<Question_View, QuestionSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Question", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<Question_View>> InitGridHeader()
        {
            return new List<GridColumn<Question_View>>{
                this.MakeGridHeader(x => x.QuestionText),
                this.MakeGridHeader(x => x.Anwser),
                this.MakeGridHeader(x => x.OptionA),
                this.MakeGridHeader(x => x.OptionB),
                this.MakeGridHeader(x => x.OptionC),
                this.MakeGridHeader(x => x.OptionD),
                this.MakeGridHeader(x => x.Name_view),
                this.MakeGridHeader(x => x.Subject),
                this.MakeGridHeader(x => x.Pars),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<Question_View> GetSearchQuery()
        {
            var query = DC.Set<Question>()
                .CheckEqual(Searcher.QuestionTypeID, x=>x.QuestionTypeID)
                .CheckContain(Searcher.Subject, x=>x.Subject)
                .CheckContain(Searcher.Pars, x=>x.Pars)
                .Select(x => new Question_View
                {
				    ID = x.ID,
                    QuestionText = x.QuestionText,
                    Anwser = x.Anwser,
                    OptionA = x.OptionA,
                    OptionB = x.OptionB,
                    OptionC = x.OptionC,
                    OptionD = x.OptionD,
                    Name_view = x.QuestionType.Name,
                    Subject = x.Subject,
                    Pars = x.Pars,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class Question_View : Question{
        [Display(Name = "题型")]
        public String Name_view { get; set; }

    }
}
