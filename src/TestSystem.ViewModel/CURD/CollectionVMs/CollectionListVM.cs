﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.CollectionVMs
{
    public partial class CollectionListVM : BasePagedListVM<Collection_View, CollectionSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("Collection", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<Collection_View>> InitGridHeader()
        {
            return new List<GridColumn<Collection_View>>{
                this.MakeGridHeader(x => x.QuestionText_view),
                this.MakeGridHeader(x => x.Name_view),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<Collection_View> GetSearchQuery()
        {
            var query = DC.Set<Collection>()
                .CheckEqual(Searcher.WebUserID, x=>x.WebUserID)
                .Select(x => new Collection_View
                {
				    ID = x.ID,
                    QuestionText_view = x.Question.QuestionText,
                    Name_view = x.WebUser.Name,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class Collection_View : Collection{
        [Display(Name = "题干")]
        public String QuestionText_view { get; set; }
        [Display(Name = "Name")]
        public String Name_view { get; set; }

    }
}
