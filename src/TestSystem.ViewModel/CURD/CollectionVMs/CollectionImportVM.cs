﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.CollectionVMs
{
    public partial class CollectionTemplateVM : BaseTemplateVM
    {
        [Display(Name = "收藏题目")]
        public ExcelPropety Question_Excel = ExcelPropety.CreateProperty<Collection>(x => x.QuestionID);
        [Display(Name = "用户")]
        public ExcelPropety WebUser_Excel = ExcelPropety.CreateProperty<Collection>(x => x.WebUserID);

	    protected override void InitVM()
        {
            Question_Excel.DataType = ColumnDataType.ComboBox;
            Question_Excel.ListItems = DC.Set<Question>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.QuestionText);
            WebUser_Excel.DataType = ColumnDataType.ComboBox;
            WebUser_Excel.ListItems = DC.Set<WebUser>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Name);
        }

    }

    public class CollectionImportVM : BaseImportVM<CollectionTemplateVM, Collection>
    {

    }

}
