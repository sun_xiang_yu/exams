﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.CollectionVMs
{
    public partial class CollectionVM : BaseCRUDVM<Collection>
    {
        public List<ComboSelectListItem> AllQuestions { get; set; }
        public List<ComboSelectListItem> AllWebUsers { get; set; }

        public CollectionVM()
        {
            SetInclude(x => x.Question);
            SetInclude(x => x.WebUser);
        }

        protected override void InitVM()
        {
            AllQuestions = DC.Set<Question>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.QuestionText);
            AllWebUsers = DC.Set<WebUser>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Name);
        }

        public override void DoAdd()
        {           
            base.DoAdd();
        }

        public override void DoEdit(bool updateAllFields = false)
        {
            base.DoEdit(updateAllFields);
        }

        public override void DoDelete()
        {
            base.DoDelete();
        }
    }
}
