﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.QuestionTypeVMs
{
    public partial class QuestionTypeListVM : BasePagedListVM<QuestionType_View, QuestionTypeSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("QuestionType", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<QuestionType_View>> InitGridHeader()
        {
            return new List<GridColumn<QuestionType_View>>{
                this.MakeGridHeader(x => x.Name),
                this.MakeGridHeader(x => x.Remark),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<QuestionType_View> GetSearchQuery()
        {
            var query = DC.Set<QuestionType>()
                .Select(x => new QuestionType_View
                {
				    ID = x.ID,
                    Name = x.Name,
                    Remark = x.Remark,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class QuestionType_View : QuestionType{

    }
}
