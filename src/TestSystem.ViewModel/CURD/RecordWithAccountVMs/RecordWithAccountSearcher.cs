﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.RecordWithAccountVMs
{
    public partial class RecordWithAccountSearcher : BaseSearcher
    {
        public List<ComboSelectListItem> AllWebUsers { get; set; }
        [Display(Name = "考试用户")]
        public Guid? WebUserID { get; set; }

        protected override void InitVM()
        {
            AllWebUsers = DC.Set<FrameworkUserBase>().GetSelectListItems(LoginUserInfo?.DataPrivileges, null, y => y.Name);
        }

    }
}
