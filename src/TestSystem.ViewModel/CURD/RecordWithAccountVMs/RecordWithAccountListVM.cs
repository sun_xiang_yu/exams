﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using TestSystem.Model;


namespace TestSystem.ViewModel.CURD.RecordWithAccountVMs
{
    public partial class RecordWithAccountListVM : BasePagedListVM<RecordWithAccount_View, RecordWithAccountSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.Create, Localizer["Create"],"CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.Edit, Localizer["Edit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.Delete, Localizer["Delete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.Details, Localizer["Details"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.BatchEdit, Localizer["BatchEdit"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.BatchDelete, Localizer["BatchDelete"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.Import, Localizer["Import"], "CURD", dialogWidth: 800),
                this.MakeStandardAction("RecordWithAccount", GridActionStandardTypesEnum.ExportExcel, Localizer["Export"], "CURD"),
            };
        }


        protected override IEnumerable<IGridColumn<RecordWithAccount_View>> InitGridHeader()
        {
            return new List<GridColumn<RecordWithAccount_View>>{
                this.MakeGridHeader(x => x.Title_view),
                this.MakeGridHeader(x => x.ParticipationTime),
                this.MakeGridHeader(x => x.Name_view),
                this.MakeGridHeader(x => x.Achievement),
                this.MakeGridHeader(x => x.QuestionId),
                this.MakeGridHeader(x => x.QuestionAnswer),
                this.MakeGridHeader(x => x.ExamineeAnswers),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<RecordWithAccount_View> GetSearchQuery()
        {
            var query = DC.Set<RecordWithAccount>()
                .CheckEqual(Searcher.WebUserID, x=>x.WebUserID)
                .Select(x => new RecordWithAccount_View
                {
				    ID = x.ID,
                    Title_view = x.ExaminationSetup.Title,
                    ParticipationTime = x.ParticipationTime,
                    Name_view = x.WebUser.Name,
                    Achievement = x.Achievement,
                    QuestionId = x.QuestionId,
                    QuestionAnswer = x.QuestionAnswer,
                    ExamineeAnswers = x.ExamineeAnswers,
                })
                .OrderBy(x => x.ID);
            return query;
        }

    }

    public class RecordWithAccount_View : RecordWithAccount{
        [Display(Name = "试卷名称")]
        public String Title_view { get; set; }
        [Display(Name = "Name")]
        public String Name_view { get; set; }

    }
}
