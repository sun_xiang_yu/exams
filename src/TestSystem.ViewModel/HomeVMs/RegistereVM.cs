﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestSystem.ViewModel.HomeVMs
{
    public class RegistereVM
    {      
        [Required(ErrorMessage = "账号不能为空")]
        [StringLength(50, ErrorMessage = "{0}stringmax{1}")]
        public string ITCode { get; set; }
        [Required(ErrorMessage = "姓名不能为空")]
        [StringLength(50, ErrorMessage = "{0}stringmax{1}")]
        public string Name { get; set; }
        [Required(ErrorMessage = "单位不能为空")]
        public Guid? UnitWorkId { get; set; }
        [Required(ErrorMessage = "第三方唯一标识获取失败")]     
        public string OpenId { get; set; }

    }
}
