﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSystem.Model.FrameworkEnumeration
{
    /// <summary>
    /// 课程状态
    /// </summary>
    public enum CourseEnum
    {
        正常,
        禁用
    }
}
