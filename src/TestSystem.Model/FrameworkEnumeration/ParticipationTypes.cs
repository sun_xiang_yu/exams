﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSystem.Model.FrameworkEnumeration
{
    /// <summary>
    /// 参与类型
    /// </summary>
    public enum ParticipationTypes
    {
        非账号,
        账号
    }
}
