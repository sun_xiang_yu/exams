﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCClient.Models;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Mvc;

namespace MVCClient.Controllers
{
    public class HomeController : BaseController
    {
        
        [Public]
        public IActionResult Index()
        {
            var user = DC.Set<FrameworkUserBase>().ToList();
            return Ok(user);
        }

       
    }
}
