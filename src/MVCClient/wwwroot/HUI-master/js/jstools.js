﻿function GetUserId() {
    var result = '';
    $.ajax({
        url: '/api/_Account/CheckUserInfo/',
        type: 'get',
        dataType: 'json',
        async: false,
        success: function (res) {

            result = res.Id;
        }
    });
    return result;
}