﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using TestSystem.Model;
using WalkingTec.Mvvm.Core;

namespace TestSystem.DataAccess
{
    public class DataContext : FrameworkContext
    {
        public DataContext(CS cs)
             : base(cs)
        {
        }

        public DataContext(string cs, DBTypeEnum dbtype, string version=null)
             : base(cs, dbtype, version)
        {
        }

        /// <summary>
        /// 题型
        /// </summary>
        public DbSet<QuestionType> QuestionTypes { get; set; }
        /// <summary>
        /// 题库
        /// </summary>
        public DbSet<Question> Questions { get; set; }
        /// <summary>
        /// 考试设置
        /// </summary>
        public DbSet<ExaminationSetup> ExaminationSetups { get; set; }
        /// <summary>
        /// 印章
        /// </summary>
        public DbSet<Seal> Seals { get; set; }
        /// <summary>
        /// 免账号参与考试记录
        /// </summary>
        public DbSet<RecordNoAccount> RecordNoAccounts { get; set; }
        /// <summary>
        /// 登录账号参与考试记录
        /// </summary>
        public DbSet<RecordWithAccount> RecordWithAccounts { get; set; }
        /// <summary>
        /// 登录用户
        /// </summary>
        public DbSet<WebUser> WebUsers { get; set; }
        /// <summary>
        /// 轮播图设置
        /// </summary>
        public DbSet<RotationChart> RotationCharts { get; set; }
        /// <summary>
        /// 试题收藏夹
        /// </summary>
        public DbSet<Collection> Collections { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public DbSet<UnitWork> UnitWorks { get; set; }
        /// <summary>
        /// 错题库
        /// </summary>
        public DbSet<WrongQuestionBank> WrongQuestionBanks { get; set; }

    }

    /// <summary>
    /// DesignTimeFactory for EF Migration, use your full connection string,
    /// EF will find this class and use the connection defined here to run Add-Migration and Update-Database
    /// </summary>
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            return new DataContext("your full connection string", DBTypeEnum.SqlServer);
        }
    }

}
