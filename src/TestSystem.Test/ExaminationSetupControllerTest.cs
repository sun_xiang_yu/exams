﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.ExaminationSetupVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class ExaminationSetupControllerTest
    {
        private ExaminationSetupController _controller;
        private string _seed;

        public ExaminationSetupControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<ExaminationSetupController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as ExaminationSetupListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(ExaminationSetupVM));

            ExaminationSetupVM vm = rv.Model as ExaminationSetupVM;
            ExaminationSetup v = new ExaminationSetup();
			
            v.ID = 35;
            v.Title = "IY8";
            v.DXNumer = 66;
            v.DXScore = 33;
            v.DSXNumer = 82;
            v.DSXScore = 32;
            v.PDNumer = 46;
            v.PDScore = 99;
            v.TestTime = 13;
            v.SealId = AddSeal();
            v.Subject = "rBI6p69";
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<ExaminationSetup>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 35);
                Assert.AreEqual(data.Title, "IY8");
                Assert.AreEqual(data.DXNumer, 66);
                Assert.AreEqual(data.DXScore, 33);
                Assert.AreEqual(data.DSXNumer, 82);
                Assert.AreEqual(data.DSXScore, 32);
                Assert.AreEqual(data.PDNumer, 46);
                Assert.AreEqual(data.PDScore, 99);
                Assert.AreEqual(data.TestTime, 13);
                Assert.AreEqual(data.Subject, "rBI6p69");
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            ExaminationSetup v = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 35;
                v.Title = "IY8";
                v.DXNumer = 66;
                v.DXScore = 33;
                v.DSXNumer = 82;
                v.DSXScore = 32;
                v.PDNumer = 46;
                v.PDScore = 99;
                v.TestTime = 13;
                v.SealId = AddSeal();
                v.Subject = "rBI6p69";
                context.Set<ExaminationSetup>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(ExaminationSetupVM));

            ExaminationSetupVM vm = rv.Model as ExaminationSetupVM;
            v = new ExaminationSetup();
            v.ID = vm.Entity.ID;
       		
            v.Title = "Tf3C90";
            v.DXNumer = 66;
            v.DXScore = 46;
            v.DSXNumer = 32;
            v.DSXScore = 6;
            v.PDNumer = 32;
            v.PDScore = 32;
            v.TestTime = 22;
            v.Subject = "4Uf2rb4";
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.Title", "");
            vm.FC.Add("Entity.DXNumer", "");
            vm.FC.Add("Entity.DXScore", "");
            vm.FC.Add("Entity.DSXNumer", "");
            vm.FC.Add("Entity.DSXScore", "");
            vm.FC.Add("Entity.PDNumer", "");
            vm.FC.Add("Entity.PDScore", "");
            vm.FC.Add("Entity.TestTime", "");
            vm.FC.Add("Entity.SealId", "");
            vm.FC.Add("Entity.Subject", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<ExaminationSetup>().FirstOrDefault();
 				
                Assert.AreEqual(data.Title, "Tf3C90");
                Assert.AreEqual(data.DXNumer, 66);
                Assert.AreEqual(data.DXScore, 46);
                Assert.AreEqual(data.DSXNumer, 32);
                Assert.AreEqual(data.DSXScore, 6);
                Assert.AreEqual(data.PDNumer, 32);
                Assert.AreEqual(data.PDScore, 32);
                Assert.AreEqual(data.TestTime, 22);
                Assert.AreEqual(data.Subject, "4Uf2rb4");
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            ExaminationSetup v = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 35;
                v.Title = "IY8";
                v.DXNumer = 66;
                v.DXScore = 33;
                v.DSXNumer = 82;
                v.DSXScore = 32;
                v.PDNumer = 46;
                v.PDScore = 99;
                v.TestTime = 13;
                v.SealId = AddSeal();
                v.Subject = "rBI6p69";
                context.Set<ExaminationSetup>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(ExaminationSetupVM));

            ExaminationSetupVM vm = rv.Model as ExaminationSetupVM;
            v = new ExaminationSetup();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<ExaminationSetup>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            ExaminationSetup v = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 35;
                v.Title = "IY8";
                v.DXNumer = 66;
                v.DXScore = 33;
                v.DSXNumer = 82;
                v.DSXScore = 32;
                v.PDNumer = 46;
                v.PDScore = 99;
                v.TestTime = 13;
                v.SealId = AddSeal();
                v.Subject = "rBI6p69";
                context.Set<ExaminationSetup>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            ExaminationSetup v1 = new ExaminationSetup();
            ExaminationSetup v2 = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 35;
                v1.Title = "IY8";
                v1.DXNumer = 66;
                v1.DXScore = 33;
                v1.DSXNumer = 82;
                v1.DSXScore = 32;
                v1.PDNumer = 46;
                v1.PDScore = 99;
                v1.TestTime = 13;
                v1.SealId = AddSeal();
                v1.Subject = "rBI6p69";
                v2.Title = "Tf3C90";
                v2.DXNumer = 66;
                v2.DXScore = 46;
                v2.DSXNumer = 32;
                v2.DSXScore = 6;
                v2.PDNumer = 32;
                v2.PDScore = 32;
                v2.TestTime = 22;
                v2.SealId = v1.SealId; 
                v2.Subject = "4Uf2rb4";
                context.Set<ExaminationSetup>().Add(v1);
                context.Set<ExaminationSetup>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(ExaminationSetupBatchVM));

            ExaminationSetupBatchVM vm = rv.Model as ExaminationSetupBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<ExaminationSetup>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as ExaminationSetupListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddSeal()
        {
            Seal v = new Seal();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.Name = "amxHfj1";
                v.SType = "Ja32HdL";
                context.Set<Seal>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
