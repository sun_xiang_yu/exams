﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.WrongQuestionBankVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class WrongQuestionBankControllerTest
    {
        private WrongQuestionBankController _controller;
        private string _seed;

        public WrongQuestionBankControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<WrongQuestionBankController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as WrongQuestionBankListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(WrongQuestionBankVM));

            WrongQuestionBankVM vm = rv.Model as WrongQuestionBankVM;
            WrongQuestionBank v = new WrongQuestionBank();
			
            v.ID = 62;
            v.QuestionID = 66;
            v.WebUserID = AddWebUser();
            v.WrongNumber = 28;
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<WrongQuestionBank>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 62);
                Assert.AreEqual(data.QuestionID, 66);
                Assert.AreEqual(data.WrongNumber, 28);
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            WrongQuestionBank v = new WrongQuestionBank();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 62;
                v.QuestionID = 66;
                v.WebUserID = AddWebUser();
                v.WrongNumber = 28;
                context.Set<WrongQuestionBank>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(WrongQuestionBankVM));

            WrongQuestionBankVM vm = rv.Model as WrongQuestionBankVM;
            v = new WrongQuestionBank();
            v.ID = vm.Entity.ID;
       		
            v.QuestionID = 44;
            v.WrongNumber = 51;
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.QuestionID", "");
            vm.FC.Add("Entity.WebUserID", "");
            vm.FC.Add("Entity.WrongNumber", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<WrongQuestionBank>().FirstOrDefault();
 				
                Assert.AreEqual(data.QuestionID, 44);
                Assert.AreEqual(data.WrongNumber, 51);
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            WrongQuestionBank v = new WrongQuestionBank();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 62;
                v.QuestionID = 66;
                v.WebUserID = AddWebUser();
                v.WrongNumber = 28;
                context.Set<WrongQuestionBank>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(WrongQuestionBankVM));

            WrongQuestionBankVM vm = rv.Model as WrongQuestionBankVM;
            v = new WrongQuestionBank();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<WrongQuestionBank>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            WrongQuestionBank v = new WrongQuestionBank();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 62;
                v.QuestionID = 66;
                v.WebUserID = AddWebUser();
                v.WrongNumber = 28;
                context.Set<WrongQuestionBank>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            WrongQuestionBank v1 = new WrongQuestionBank();
            WrongQuestionBank v2 = new WrongQuestionBank();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 62;
                v1.QuestionID = 66;
                v1.WebUserID = AddWebUser();
                v1.WrongNumber = 28;
                v2.QuestionID = 44;
                v2.WebUserID = v1.WebUserID; 
                v2.WrongNumber = 51;
                context.Set<WrongQuestionBank>().Add(v1);
                context.Set<WrongQuestionBank>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(WrongQuestionBankBatchVM));

            WrongQuestionBankBatchVM vm = rv.Model as WrongQuestionBankBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<WrongQuestionBank>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as WrongQuestionBankListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddUnitWork()
        {
            UnitWork v = new UnitWork();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.UnitWorkName = "cdzH7mOW";
                context.Set<UnitWork>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Guid AddWebUser()
        {
            WebUser v = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.UnitWorkID = AddUnitWork();
                v.ITCode = "7sDJ";
                v.Password = "Fp7";
                v.Name = "56ZZo";
                context.Set<WebUser>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
