﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.RecordWithAccountVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class RecordWithAccountControllerTest
    {
        private RecordWithAccountController _controller;
        private string _seed;

        public RecordWithAccountControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<RecordWithAccountController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as RecordWithAccountListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(RecordWithAccountVM));

            RecordWithAccountVM vm = rv.Model as RecordWithAccountVM;
            RecordWithAccount v = new RecordWithAccount();
			
            v.ID = 66;
            v.ExaminationSetupID = AddExaminationSetup();
            v.WebUserID = AddWebUser();
            v.Achievement = 99;
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<RecordWithAccount>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 66);
                Assert.AreEqual(data.Achievement, 99);
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            RecordWithAccount v = new RecordWithAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 66;
                v.ExaminationSetupID = AddExaminationSetup();
                v.WebUserID = AddWebUser();
                v.Achievement = 99;
                context.Set<RecordWithAccount>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(RecordWithAccountVM));

            RecordWithAccountVM vm = rv.Model as RecordWithAccountVM;
            v = new RecordWithAccount();
            v.ID = vm.Entity.ID;
       		
            v.Achievement = 55;
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.ExaminationSetupID", "");
            vm.FC.Add("Entity.WebUserID", "");
            vm.FC.Add("Entity.Achievement", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<RecordWithAccount>().FirstOrDefault();
 				
                Assert.AreEqual(data.Achievement, 55);
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            RecordWithAccount v = new RecordWithAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 66;
                v.ExaminationSetupID = AddExaminationSetup();
                v.WebUserID = AddWebUser();
                v.Achievement = 99;
                context.Set<RecordWithAccount>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(RecordWithAccountVM));

            RecordWithAccountVM vm = rv.Model as RecordWithAccountVM;
            v = new RecordWithAccount();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<RecordWithAccount>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            RecordWithAccount v = new RecordWithAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 66;
                v.ExaminationSetupID = AddExaminationSetup();
                v.WebUserID = AddWebUser();
                v.Achievement = 99;
                context.Set<RecordWithAccount>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            RecordWithAccount v1 = new RecordWithAccount();
            RecordWithAccount v2 = new RecordWithAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 66;
                v1.ExaminationSetupID = AddExaminationSetup();
                v1.WebUserID = AddWebUser();
                v1.Achievement = 99;
                v2.ExaminationSetupID = v1.ExaminationSetupID; 
                v2.WebUserID = v1.WebUserID; 
                v2.Achievement = 55;
                context.Set<RecordWithAccount>().Add(v1);
                context.Set<RecordWithAccount>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(RecordWithAccountBatchVM));

            RecordWithAccountBatchVM vm = rv.Model as RecordWithAccountBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<RecordWithAccount>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as RecordWithAccountListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddSeal()
        {
            Seal v = new Seal();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.Name = "S95C0";
                v.SType = "ikcegTDN";
                context.Set<Seal>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Int32 AddExaminationSetup()
        {
            ExaminationSetup v = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ID = 88;
                v.Title = "p89";
                v.DXNumer = 63;
                v.DXScore = 72;
                v.DSXNumer = 73;
                v.DSXScore = 1;
                v.PDNumer = 82;
                v.PDScore = 5;
                v.TestTime = 46;
                v.SealId = AddSeal();
                v.Subject = "ACg";
                context.Set<ExaminationSetup>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Guid AddWebUser()
        {
            WebUser v = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ITCode = "IIh";
                v.Password = "xXdnma";
                v.Name = "vgjWh";
                context.Set<WebUser>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
