﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.WebUserVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class WebUserControllerTest
    {
        private WebUserController _controller;
        private string _seed;

        public WebUserControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<WebUserController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as WebUserListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(WebUserVM));

            WebUserVM vm = rv.Model as WebUserVM;
            WebUser v = new WebUser();
			
            v.UnitWorkID = AddUnitWork();
            v.ITCode = "vXxAKs2zc";
            v.Password = "HmLnEW4T";
            v.Name = "5POXd5IIF";
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<WebUser>().FirstOrDefault();
				
                Assert.AreEqual(data.ITCode, "vXxAKs2zc");
                Assert.AreEqual(data.Password, "HmLnEW4T");
                Assert.AreEqual(data.Name, "5POXd5IIF");
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            WebUser v = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.UnitWorkID = AddUnitWork();
                v.ITCode = "vXxAKs2zc";
                v.Password = "HmLnEW4T";
                v.Name = "5POXd5IIF";
                context.Set<WebUser>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(WebUserVM));

            WebUserVM vm = rv.Model as WebUserVM;
            v = new WebUser();
            v.ID = vm.Entity.ID;
       		
            v.ITCode = "fHh1X";
            v.Password = "RDM";
            v.Name = "rXTzxmiMj";
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.UnitWorkID", "");
            vm.FC.Add("Entity.ITCode", "");
            vm.FC.Add("Entity.Password", "");
            vm.FC.Add("Entity.Name", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<WebUser>().FirstOrDefault();
 				
                Assert.AreEqual(data.ITCode, "fHh1X");
                Assert.AreEqual(data.Password, "RDM");
                Assert.AreEqual(data.Name, "rXTzxmiMj");
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            WebUser v = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.UnitWorkID = AddUnitWork();
                v.ITCode = "vXxAKs2zc";
                v.Password = "HmLnEW4T";
                v.Name = "5POXd5IIF";
                context.Set<WebUser>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(WebUserVM));

            WebUserVM vm = rv.Model as WebUserVM;
            v = new WebUser();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<WebUser>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            WebUser v = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.UnitWorkID = AddUnitWork();
                v.ITCode = "vXxAKs2zc";
                v.Password = "HmLnEW4T";
                v.Name = "5POXd5IIF";
                context.Set<WebUser>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            WebUser v1 = new WebUser();
            WebUser v2 = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.UnitWorkID = AddUnitWork();
                v1.ITCode = "vXxAKs2zc";
                v1.Password = "HmLnEW4T";
                v1.Name = "5POXd5IIF";
                v2.UnitWorkID = v1.UnitWorkID; 
                v2.ITCode = "fHh1X";
                v2.Password = "RDM";
                v2.Name = "rXTzxmiMj";
                context.Set<WebUser>().Add(v1);
                context.Set<WebUser>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(WebUserBatchVM));

            WebUserBatchVM vm = rv.Model as WebUserBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<WebUser>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as WebUserListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddUnitWork()
        {
            UnitWork v = new UnitWork();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.UnitWorkName = "7Xmj";
                context.Set<UnitWork>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
