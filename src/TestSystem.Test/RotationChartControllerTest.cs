﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.RotationChartVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class RotationChartControllerTest
    {
        private RotationChartController _controller;
        private string _seed;

        public RotationChartControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<RotationChartController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as RotationChartListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(RotationChartVM));

            RotationChartVM vm = rv.Model as RotationChartVM;
            RotationChart v = new RotationChart();
			
            v.ID = 34;
            v.ExaminationSetupID = AddExaminationSetup();
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<RotationChart>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 34);
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            RotationChart v = new RotationChart();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 34;
                v.ExaminationSetupID = AddExaminationSetup();
                context.Set<RotationChart>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(RotationChartVM));

            RotationChartVM vm = rv.Model as RotationChartVM;
            v = new RotationChart();
            v.ID = vm.Entity.ID;
       		
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.ExaminationSetupID", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<RotationChart>().FirstOrDefault();
 				
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            RotationChart v = new RotationChart();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 34;
                v.ExaminationSetupID = AddExaminationSetup();
                context.Set<RotationChart>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(RotationChartVM));

            RotationChartVM vm = rv.Model as RotationChartVM;
            v = new RotationChart();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<RotationChart>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            RotationChart v = new RotationChart();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 34;
                v.ExaminationSetupID = AddExaminationSetup();
                context.Set<RotationChart>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            RotationChart v1 = new RotationChart();
            RotationChart v2 = new RotationChart();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 34;
                v1.ExaminationSetupID = AddExaminationSetup();
                v2.ExaminationSetupID = v1.ExaminationSetupID; 
                context.Set<RotationChart>().Add(v1);
                context.Set<RotationChart>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(RotationChartBatchVM));

            RotationChartBatchVM vm = rv.Model as RotationChartBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<RotationChart>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as RotationChartListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddSeal()
        {
            Seal v = new Seal();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.Name = "vicIed";
                v.SType = "5Kn";
                context.Set<Seal>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Int32 AddExaminationSetup()
        {
            ExaminationSetup v = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ID = 13;
                v.Title = "fNqz1";
                v.DXNumer = 98;
                v.DXScore = 84;
                v.DSXNumer = 13;
                v.DSXScore = 55;
                v.PDNumer = 49;
                v.PDScore = 89;
                v.TestTime = 54;
                v.SealId = AddSeal();
                v.Subject = "MOy";
                context.Set<ExaminationSetup>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
