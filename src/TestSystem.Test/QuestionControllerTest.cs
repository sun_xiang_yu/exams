﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.QuestionVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class QuestionControllerTest
    {
        private QuestionController _controller;
        private string _seed;

        public QuestionControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<QuestionController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as QuestionListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionVM));

            QuestionVM vm = rv.Model as QuestionVM;
            Question v = new Question();
			
            v.ID = 79;
            v.QuestionText = "sQRWusbP";
            v.Anwser = "kVHzix";
            v.OptionA = "7Vm";
            v.OptionB = "SMaRD";
            v.QuestionTypeID = AddQuestionType();
            v.Subject = "Yh3k";
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Question>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 79);
                Assert.AreEqual(data.QuestionText, "sQRWusbP");
                Assert.AreEqual(data.Anwser, "kVHzix");
                Assert.AreEqual(data.OptionA, "7Vm");
                Assert.AreEqual(data.OptionB, "SMaRD");
                Assert.AreEqual(data.Subject, "Yh3k");
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            Question v = new Question();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 79;
                v.QuestionText = "sQRWusbP";
                v.Anwser = "kVHzix";
                v.OptionA = "7Vm";
                v.OptionB = "SMaRD";
                v.QuestionTypeID = AddQuestionType();
                v.Subject = "Yh3k";
                context.Set<Question>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionVM));

            QuestionVM vm = rv.Model as QuestionVM;
            v = new Question();
            v.ID = vm.Entity.ID;
       		
            v.QuestionText = "zOLw1fr";
            v.Anwser = "m594wi";
            v.OptionA = "pb8td";
            v.OptionB = "ZXG0";
            v.Subject = "F6kSNH";
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.QuestionText", "");
            vm.FC.Add("Entity.Anwser", "");
            vm.FC.Add("Entity.OptionA", "");
            vm.FC.Add("Entity.OptionB", "");
            vm.FC.Add("Entity.QuestionTypeID", "");
            vm.FC.Add("Entity.Subject", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Question>().FirstOrDefault();
 				
                Assert.AreEqual(data.QuestionText, "zOLw1fr");
                Assert.AreEqual(data.Anwser, "m594wi");
                Assert.AreEqual(data.OptionA, "pb8td");
                Assert.AreEqual(data.OptionB, "ZXG0");
                Assert.AreEqual(data.Subject, "F6kSNH");
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            Question v = new Question();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 79;
                v.QuestionText = "sQRWusbP";
                v.Anwser = "kVHzix";
                v.OptionA = "7Vm";
                v.OptionB = "SMaRD";
                v.QuestionTypeID = AddQuestionType();
                v.Subject = "Yh3k";
                context.Set<Question>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionVM));

            QuestionVM vm = rv.Model as QuestionVM;
            v = new Question();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<Question>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            Question v = new Question();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 79;
                v.QuestionText = "sQRWusbP";
                v.Anwser = "kVHzix";
                v.OptionA = "7Vm";
                v.OptionB = "SMaRD";
                v.QuestionTypeID = AddQuestionType();
                v.Subject = "Yh3k";
                context.Set<Question>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            Question v1 = new Question();
            Question v2 = new Question();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 79;
                v1.QuestionText = "sQRWusbP";
                v1.Anwser = "kVHzix";
                v1.OptionA = "7Vm";
                v1.OptionB = "SMaRD";
                v1.QuestionTypeID = AddQuestionType();
                v1.Subject = "Yh3k";
                v2.QuestionText = "zOLw1fr";
                v2.Anwser = "m594wi";
                v2.OptionA = "pb8td";
                v2.OptionB = "ZXG0";
                v2.QuestionTypeID = v1.QuestionTypeID; 
                v2.Subject = "F6kSNH";
                context.Set<Question>().Add(v1);
                context.Set<Question>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionBatchVM));

            QuestionBatchVM vm = rv.Model as QuestionBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<Question>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as QuestionListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Int32 AddQuestionType()
        {
            QuestionType v = new QuestionType();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ID = 38;
                v.Name = "60DrVy2V";
                context.Set<QuestionType>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
