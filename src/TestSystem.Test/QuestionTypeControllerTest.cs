﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.QuestionTypeVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class QuestionTypeControllerTest
    {
        private QuestionTypeController _controller;
        private string _seed;

        public QuestionTypeControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<QuestionTypeController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as QuestionTypeListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionTypeVM));

            QuestionTypeVM vm = rv.Model as QuestionTypeVM;
            QuestionType v = new QuestionType();
			
            v.ID = 98;
            v.Name = "E8VnI3RSp";
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<QuestionType>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 98);
                Assert.AreEqual(data.Name, "E8VnI3RSp");
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            QuestionType v = new QuestionType();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 98;
                v.Name = "E8VnI3RSp";
                context.Set<QuestionType>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionTypeVM));

            QuestionTypeVM vm = rv.Model as QuestionTypeVM;
            v = new QuestionType();
            v.ID = vm.Entity.ID;
       		
            v.Name = "rWQvXca";
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.Name", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<QuestionType>().FirstOrDefault();
 				
                Assert.AreEqual(data.Name, "rWQvXca");
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            QuestionType v = new QuestionType();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 98;
                v.Name = "E8VnI3RSp";
                context.Set<QuestionType>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionTypeVM));

            QuestionTypeVM vm = rv.Model as QuestionTypeVM;
            v = new QuestionType();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<QuestionType>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            QuestionType v = new QuestionType();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 98;
                v.Name = "E8VnI3RSp";
                context.Set<QuestionType>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            QuestionType v1 = new QuestionType();
            QuestionType v2 = new QuestionType();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 98;
                v1.Name = "E8VnI3RSp";
                v2.Name = "rWQvXca";
                context.Set<QuestionType>().Add(v1);
                context.Set<QuestionType>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(QuestionTypeBatchVM));

            QuestionTypeBatchVM vm = rv.Model as QuestionTypeBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<QuestionType>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as QuestionTypeListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }


    }
}
