﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.RecordNoAccountVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class RecordNoAccountControllerTest
    {
        private RecordNoAccountController _controller;
        private string _seed;

        public RecordNoAccountControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<RecordNoAccountController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as RecordNoAccountListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(RecordNoAccountVM));

            RecordNoAccountVM vm = rv.Model as RecordNoAccountVM;
            RecordNoAccount v = new RecordNoAccount();
			
            v.ID = 67;
            v.ExaminationSetupID = AddExaminationSetup();
            v.UserName = "cFzdEw1sj";
            v.UnitWork = "nOwO";
            v.Achievement = 61;
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<RecordNoAccount>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 67);
                Assert.AreEqual(data.UserName, "cFzdEw1sj");
                Assert.AreEqual(data.UnitWork, "nOwO");
                Assert.AreEqual(data.Achievement, 61);
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            RecordNoAccount v = new RecordNoAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 67;
                v.ExaminationSetupID = AddExaminationSetup();
                v.UserName = "cFzdEw1sj";
                v.UnitWork = "nOwO";
                v.Achievement = 61;
                context.Set<RecordNoAccount>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(RecordNoAccountVM));

            RecordNoAccountVM vm = rv.Model as RecordNoAccountVM;
            v = new RecordNoAccount();
            v.ID = vm.Entity.ID;
       		
            v.UserName = "9NJd";
            v.UnitWork = "0ND3u4zoR";
            v.Achievement = 44;
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.ExaminationSetupID", "");
            vm.FC.Add("Entity.UserName", "");
            vm.FC.Add("Entity.UnitWork", "");
            vm.FC.Add("Entity.Achievement", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<RecordNoAccount>().FirstOrDefault();
 				
                Assert.AreEqual(data.UserName, "9NJd");
                Assert.AreEqual(data.UnitWork, "0ND3u4zoR");
                Assert.AreEqual(data.Achievement, 44);
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            RecordNoAccount v = new RecordNoAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 67;
                v.ExaminationSetupID = AddExaminationSetup();
                v.UserName = "cFzdEw1sj";
                v.UnitWork = "nOwO";
                v.Achievement = 61;
                context.Set<RecordNoAccount>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(RecordNoAccountVM));

            RecordNoAccountVM vm = rv.Model as RecordNoAccountVM;
            v = new RecordNoAccount();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<RecordNoAccount>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            RecordNoAccount v = new RecordNoAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 67;
                v.ExaminationSetupID = AddExaminationSetup();
                v.UserName = "cFzdEw1sj";
                v.UnitWork = "nOwO";
                v.Achievement = 61;
                context.Set<RecordNoAccount>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            RecordNoAccount v1 = new RecordNoAccount();
            RecordNoAccount v2 = new RecordNoAccount();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 67;
                v1.ExaminationSetupID = AddExaminationSetup();
                v1.UserName = "cFzdEw1sj";
                v1.UnitWork = "nOwO";
                v1.Achievement = 61;
                v2.ExaminationSetupID = v1.ExaminationSetupID; 
                v2.UserName = "9NJd";
                v2.UnitWork = "0ND3u4zoR";
                v2.Achievement = 44;
                context.Set<RecordNoAccount>().Add(v1);
                context.Set<RecordNoAccount>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(RecordNoAccountBatchVM));

            RecordNoAccountBatchVM vm = rv.Model as RecordNoAccountBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<RecordNoAccount>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as RecordNoAccountListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddSeal()
        {
            Seal v = new Seal();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.Name = "6VIrOan3I";
                v.SType = "6ale7s";
                context.Set<Seal>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Int32 AddExaminationSetup()
        {
            ExaminationSetup v = new ExaminationSetup();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ID = 34;
                v.Title = "cv8d2";
                v.DXNumer = 70;
                v.DXScore = 49;
                v.DSXNumer = 83;
                v.DSXScore = 0;
                v.PDNumer = 44;
                v.PDScore = 86;
                v.TestTime = 67;
                v.SealId = AddSeal();
                v.Subject = "9ohuW0y";
                context.Set<ExaminationSetup>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
