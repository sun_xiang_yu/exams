﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using TestSystem.Controllers;
using TestSystem.ViewModel.CURD.CollectionVMs;
using TestSystem.Model;
using TestSystem.DataAccess;

namespace TestSystem.Test
{
    [TestClass]
    public class CollectionControllerTest
    {
        private CollectionController _controller;
        private string _seed;

        public CollectionControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<CollectionController>(_seed, "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search(rv.Model as CollectionListVM);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(CollectionVM));

            CollectionVM vm = rv.Model as CollectionVM;
            Collection v = new Collection();
			
            v.ID = 62;
            v.QuestionID = AddQuestion();
            v.WebUserID = AddWebUser();
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Collection>().FirstOrDefault();
				
                Assert.AreEqual(data.ID, 62);
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            Collection v = new Collection();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.ID = 62;
                v.QuestionID = AddQuestion();
                v.WebUserID = AddWebUser();
                context.Set<Collection>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(CollectionVM));

            CollectionVM vm = rv.Model as CollectionVM;
            v = new Collection();
            v.ID = vm.Entity.ID;
       		
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.ID", "");
            vm.FC.Add("Entity.QuestionID", "");
            vm.FC.Add("Entity.WebUserID", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Collection>().FirstOrDefault();
 				
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            Collection v = new Collection();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.ID = 62;
                v.QuestionID = AddQuestion();
                v.WebUserID = AddWebUser();
                context.Set<Collection>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(CollectionVM));

            CollectionVM vm = rv.Model as CollectionVM;
            v = new Collection();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<Collection>().Count(), 0);
            }

        }


        [TestMethod]
        public void DetailsTest()
        {
            Collection v = new Collection();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.ID = 62;
                v.QuestionID = AddQuestion();
                v.WebUserID = AddWebUser();
                context.Set<Collection>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchDeleteTest()
        {
            Collection v1 = new Collection();
            Collection v2 = new Collection();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.ID = 62;
                v1.QuestionID = AddQuestion();
                v1.WebUserID = AddWebUser();
                v2.QuestionID = v1.QuestionID; 
                v2.WebUserID = v1.WebUserID; 
                context.Set<Collection>().Add(v1);
                context.Set<Collection>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(CollectionBatchVM));

            CollectionBatchVM vm = rv.Model as CollectionBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                Assert.AreEqual(context.Set<Collection>().Count(), 0);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as CollectionListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Int32 AddQuestionType()
        {
            QuestionType v = new QuestionType();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ID = 63;
                v.Name = "9trNuw";
                context.Set<QuestionType>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Int32 AddQuestion()
        {
            Question v = new Question();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ID = 50;
                v.QuestionText = "oJqnLIYe0";
                v.Anwser = "JgS";
                v.OptionA = "qGNf";
                v.OptionB = "zQ55y";
                v.QuestionTypeID = AddQuestionType();
                v.Subject = "krBHJ";
                context.Set<Question>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }

        private Guid AddWebUser()
        {
            WebUser v = new WebUser();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {

                v.ITCode = "UhOOQR";
                v.Password = "YqfQtg";
                v.Name = "MpEy";
                context.Set<WebUser>().Add(v);
                context.SaveChanges();
            }
            return v.ID;
        }


    }
}
