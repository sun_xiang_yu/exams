﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Tools
{
    public static class HttpHelper
    {

        /// <summary>
        /// 用于URL传参的http请求
        /// </summary>
        /// <param name="method">get/post</param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string SendHttp(string method, string url)
        {
            try
            {
                HttpWebRequest request;
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                if (method == "get")
                {
                    request.UserAgent = "Code Sample Web Client";
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myreader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                string responseText = myreader.ReadToEnd();
                myreader.Dispose();
                myResponseStream.Dispose();
                response.Dispose();


                return responseText;
            }
            catch (Exception ex)
            {
                throw ex;
               
            }

           
            
        }
    }
}
