# ExamsWTM

#### 介绍
WTM考试系统

#### 软件架构
软件架构说明
源码位于src文件夹下
TestSystem项目为WTM官方https://wtmdoc.walkingtec.cn/生成项目模板 用于制作考试系统后
MVCClient项目为ASP.NET Core原生模板  用于制作微信嵌入式网页客户端


#### 安装教程

1.  打开src文件后直接点开TestSystem.sln项目解决方案
2.  在TestSystem、和MVCClient修改项目连接字符串 目前采用的是sqlserver 如果想用MySQL或者其他。在TestSystem项目中按照WTM规则进行设置，MVCClient只需修改连接字符串即可。



